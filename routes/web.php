<?php

use App\Http\Controllers\DashboardController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\AboutController;
use App\Http\Controllers\Category_listController;
use App\Http\Controllers\createCategory;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return view('welcome');
});
Route::get('/dashboard', function () {
    return view('dashboard');
});
Route::get('/product', function () {
    return view('admin.product');
});
Route::get('/category_list', function () {
    return view('admin.category_list');
});
Route::get('/about', function () {
    return view('admin.about');
});
Route::get('/edit_course', function () {
    return view('admin.edit_course');
});
*/
Route::get('/dashboard',[DashboardController::class,'Dashboard']);
Route::get('/product',[ProductController::class,'Product'])->name('product');

Route::get('/about',[AboutController::class,'About']);
Route::get('/edit_course/{id}',[ProductController::class,'Edit_course'])->name('product.edit');
Route::put('/update_course/{id}',[ProductController::class,'update'])->name('course.update');
Route::get('/trash/delete',[ProductController::class,'trash'])->name('trash.course');
Route::get('/permanent/delete',[ProductController::class,'trash'])->name('permanent.delete');
Route::get('/restore/course/{id}',[ProductController::class,'restore'])->name('restore.course');
Route::get('/delete/{id}',[ProductController::class,'Delete'])->name('product.delete');
Route::get('/view/{id}',[ProductController::class,'View'])->name('product.view');
Route::get('/add_course/',[ProductController::class,'Add_course'])->name('add.course');
Route::post('/create_course/',[ProductController::class,'craeteCourse'])->name('create.course');
/* Category */
Route::get('/add_category/',[createCategory::class,'addCategory'])->name('add.category');
Route::post('/create_category/',[createCategory::class,'craeteCategory'])->name('create.category');
// Route::get('/category_list',[createCategory::class,'Category_list'])->name('');
Route::get('/category_list',[createCategory::class,'Category_list'])->name('Category_list');
Route::get('/categorydelete/{id}',[createCategory::class,'Delete'])->name('category.delete');