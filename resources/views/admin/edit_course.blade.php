<x-master>
    <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">

        <body>
            <section id="add_category" class="container  bg-light shadow mt-5 p-5 w-50">
                <h3 class="mb-3">Edit Course</h3>
                <div class="edit_course">
                    <form action="{{ route('course.update',['id'=> $course->id]) }}" method="post">
                        @csrf
                        @method('put')
                        <x-from.input name="name" :value="$course->name" id="name" type='text' />
                        <x-from.input name="email" :value="$course->email" id="email" type='email' />
                        <x-from.input name="password" :value="$course->password" id="password" type='password' />
                        <button type="submit" class="btn btn-primary w-25">Update</button>
                    </form>
                </div>
            </section>

    </main>
</x-master>
