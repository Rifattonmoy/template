<x-master>

  
            <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
          <section
            id="add_category"
            class="container bg-light shadow mt-5 p-5"
          >
            <h3 class="mb-3">Add Course</h3>
            <div class="add_course">
              <form action="{{ route('create.course') }}" method="post">
                @csrf
                <x-from.input name="name" id="name" type='text'/>
                <x-from.input name="email" id="email" type='email'/>
                <x-from.input name="password" id="password" type='password'/>
                {{-- <div class="mb-3">
                  <label for="floatingTextarea">Description</label>
                  <textarea
                    class="form-control"
                    name="description"
                    placeholder=""
                    id="floatingTextarea"
                  ></textarea>
                </div> --}}
                <button
                  type="submit"
                  class="btn btn-primary w-25"
                  >Submit</button
                >
              </form>
            </div>
          </section>
            </main>
</x-master>
    

    