<x-master>
    <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
           
                <section id="add_category" class="container  bg-light shadow mt-5 p-5">
                    <h3 class="mb-3">Add Category</h3>
                    <div class="add_category ">
                        <form action="{{ route('create.category') }}" method="post">
                            @csrf
                            <div class="mb-3">
                                <label for="name" class="form-label"> Name</label>
                                <input type="text" name="name" class="form-control" id="name"
                                    required />
                            </div>
                            <div class="mb-3">
                                <label for="email" class="form-label"> Email</label>
                                <input type="email" name="email" class="form-control" id="email"
                                    required />
                            </div>
                            
                            <button type="submit" class="btn btn-primary w-25">Submit</button>
                        </form>
                    </div>
                </section>

            </main>
        </x-master>