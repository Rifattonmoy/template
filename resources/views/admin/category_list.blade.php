<x-master>
    <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
          <section id="category" class="container bg-light mt-5 p-5 shadow">
            <div class="d-flex justify-content-between">
              <h3 class="pb-3">Category</h3>
              @if (session('message'))
              <small class="text-danger">{{ session('message') }}</small>
          @endif
              <a
                href="{{ route('add.category') }}"

                class="btn btn-primary"
                >Add Category</a
              >
            </div>
            <table class="table table-striped p-5">
              <thead>
                <tr>
                  <th scope="col">Category Name</th>
                  <th scope="col">Description</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
              @foreach ($categories as $category)
              <tbody>
                <tr>
                  <th scope="row">{{ $category->name }}</th>
                  <td>{{ $category->email }}</td>
                  
                  <td>
                    <a
                      href="./view_category.html"
                      type="button"
                      class="btn btn-info"
                      >View</a
                    >
                    <a
                      href="./edit_category.html"
                      type="button"
                      class="btn btn-success"
                      >Edit</a
                    >
                    <a href="{{ route('category.delete',['id'=>$category->id]) }}" type="button" class="btn btn-danger">Delete</a>
                  </td>
                </tr>

              </tbody>
              @endforeach
            </table>
          </section>
        </main>
</x-master>

   