<x-master>
<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
    
              <section id="course" class="container bg-light mt-5 p-5 shadow">
                @if (session('message'))
                    <small class="text-danger">{{ session('message') }}</small>
                @endif
                <div class="d-flex justify-content-between">
                  <h3>All Trash Course </h3>
                  
                  <a href="{{ route('product') }}" type="button" class="btn btn-danger"
                    >Back</a
                  >
                </div>
                <table class="table table-striped p-5">
                  <thead>
                    <tr>
                      <th scope="col">Course Name</th>
                      <th scope="col">Email</th>
                      <th scope="col">Password</th>
                      {{-- <th scope="col">Instructor</th> --}}
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  @foreach ($courses as $course)
                  <tbody>
                    <tr>
                      <th scope="row">{{ $course->name }}</th>
                      <td>{{ $course->email }}</td>
                      <td>{{ $course->password }}</td>
                      <td>
                        <a
                          href="{{ route('product.view',['id'=>$course->id]) }}"
                          type="button"
                          class="btn btn-info"
                          >View</a
                        >
                        <a
                          href="{{ route('restore.course',['id'=>$course->id]) }}"
                          type="button"
                          class="btn btn-success"
                          >Restore</a
                        >
                        <a href="{{ route('permanent.delete',['id'=>$course->id]) }}" type="button" class="btn btn-danger">Permanent Delete</a>
                      </td>
                    </tr>
                  </tbody>
                  @endforeach
                  
                </table>
              </section>
           
  </main>
</x-master>
