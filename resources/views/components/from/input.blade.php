@props(['name', 'id', 'type', 'value'=>''])
<div class="mb-3">
    <label for="{{ $id }}" class="form-label">{{ ucwords($name) }}</label>
    <input
      type="{{ $type }}"
      name="{{ $name }}"
      value="{{ $value }}"
      {{ $attributes->merge(['class'=>'form-control']) }}
      id="{{ $id }}"
    />
  </div>