<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class createCategory extends Controller
{
    public function Category_list()
    { {
            $categories = Category::all();
            return view('admin.category_list', compact('categories'));
        }
    }
    public function Delete($id)
    {
        $category = Category::find($id);
        $category->delete();
        return redirect()->route('Category_list')->withMessage('Category Deleted successfully');
    }
    public function addCategory()
    {
        return view('admin.create_category');
    }
    public function craeteCategory(Request $request)
    {
        Category::create([
            'name' => $request->name,
            'email' => $request->email,

        ]);
        return redirect()->route('Category_list')->withMessage('Category Create successfully');
    }
}
