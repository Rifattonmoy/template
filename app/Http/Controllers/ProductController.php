<?php

namespace App\Http\Controllers;

use App\Models\Course;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function Product()
    {
        $courses = Course::all();
        return view('admin.product', compact('courses'));
    }
    public function Delete($id)
    {
        $course = Course::find($id);
        $course->delete();
        return redirect()->route('product')->withMessage('Course Deleted successfully');
    }
    public function Edit_course($id)
    {
        $course = Course::find($id);
        return view('admin.edit_course', compact('course'));
    }
    public function update(Request $request, $id)
    {
        $course = Course::find($id);
        $course->update([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
        ]);
        return redirect()->route('product')->withMessage('Course updated successfully');
    }
    public function View($id)
    {
        $course = Course::find($id);
        return view('admin.view', compact('course'));
    }
    public function Add_course()
    {
        return view('admin.add_course');
    }
    public function craeteCourse(Request $request)
    {
        Course::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
        ]);
        return redirect()->route('product')->withMessage('Course created successfully');
    }
    public function trash()
    {
        $courses = Course::onlyTrashed()->get();
        return view('admin.trash', compact('courses'));
    }
    public function permanetDelete($id)
    {
        $courses = Course::onlyTrashed()->find($id);
        $courses->forceDelete();
        return redirect()->route('trash.course')->withMessage('Course Permanet delete successfully');
    }
    public function restore($id)
    {
        $courses = Course::onlyTrashed()->find($id);
        $courses->restore();
        return redirect()->route('product')->withMessage('Course restore successfully');
    }
}
