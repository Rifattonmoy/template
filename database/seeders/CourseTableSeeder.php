<?php

namespace Database\Seeders;

use App\Models\Course;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CourseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Course::create([
            'name'=>'php',
            'email'=>'xyz@gmail.com',
            'password'=>'1234',
        ]);
        Course::create([
            'name'=>'laravel',
            'email'=>'afz@gmail.com',
            'password'=>'1114',
        ]);
    }
}
